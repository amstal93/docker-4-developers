# example

Example Maven project generated using `maven-archetype-quickstart`

[https://jitpack.io/#jitpack/maven-simple](https://jitpack.io/#jitpack/maven-simple)

[![Release](https://jitpack.io/v/jitpack/maven-simple.svg)](https://jitpack.io/#jitpack/maven-simple)

Used to demo building in a Docker container
```
docker build .
docker run ${HASH_OF_CONTAINER_BUILD}
```

